<?php
/**
 * @file
 * Password hint module file.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function password_hint_form_user_login_alter(&$form, &$form_state) {
  _password_hint_login_form_alter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function password_hint_form_user_login_block_alter(&$form, &$form_state) {
  _password_hint_login_form_alter($form, $form_state);
}

/**
 * Alters login forms to include an ajax callback for requesting password hints.
 */
function _password_hint_login_form_alter(&$form, &$form_state) {
  $hint = array(
    'get_hint' => array(
      '#type' => 'button',
      '#theme' => 'password_hint_button',
      // Prevent theme_button from running.
      '#theme_wrappers' => array(),
      '#value' => t('Get hint'),
      '#ajax' => array(
        'callback' => 'password_hint_ajax_get_hint',
        'wrapper' => 'password-hint-hint',
        'effect' => 'slide',
      ),
      // This a) allows logintoboggan to alter the username from an email, and
      // b) allows #required validation on the name field.
      '#limit_validation_errors' => array(array('name')),
      '#validate' => array('password_hint_login_form_validate'),
    ),
    'hint' => array(
      '#markup' => '<div id="password-hint-hint"></div>',
    ),
  );

  password_hint_element_insert_after($form, array('hint' => $hint), 'pass');
}

/**
 * Form validation callback when checking a password hint.
 */
function password_hint_login_form_validate(&$form, &$form_state) {
  foreach ($form['#validate'] as $function) {
    // Run all form validation functions except user_login_final_validate()
    // Because that set's the 'no account found' error and registers flood
    // events that we don't need to trigger for hints.
    if ($function !== 'user_login_final_validate') {
      $function($form, $form_state);
    }
  }
}

/**
 * AJAX callback for retrieving password hints and returning them to the user.
 */
function password_hint_ajax_get_hint(&$form, &$form_state) {
  $name = $form_state['values']['name'];
  $output = array(
    '#prefix' => '<div id="password-hint-hint">',
    '#suffix' => '</div>',
  );

  // Standard form validation applies otherwise.
  if ($name) {
    // Limit an IP address to 10 hint requests per hour.
    if (!flood_is_allowed(__FUNCTION__, variable_get('password_hint_flood_limit', 10))) {
      watchdog('password_hint', 'Flood limit reached for password hints.');
      return $output;
    }
    flood_register_event(__FUNCTION__);

    $user = user_load_by_name($name);

    if ($user) {
      $items = field_get_items('user', $user, 'password_hint');
      if ($items) {
        $hint = $items[0]['value'];
        $output['hint'] = array(
          '#theme' => 'password_hint',
          '#hint' => $hint,
        );
      }
    }
    else {
      form_set_error('name', t('There was no hint found for %name.', array('%name' => $name)));
    }
  }
  else {
    form_set_error('name', t('!name field is required.', array('!name' => $form['name']['#title'])));
  }

  return $output;
}

/**
 * Implements hook_theme().
 */
function password_hint_theme() {
  return array(
    'password_hint' => array(
      'variables' => array('hint' => ''),
    ),
    'password_hint_button' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Return HTML for password hints when they are displayed to a user.
 *
 * @ingroup themeable
 */
function theme_password_hint($variables) {
  drupal_set_message(t('Hint: %hint', array('%hint' => $variables['hint'])));
}

/**
 * Returns HTML for the "get hint" button to be displayed on the login form.
 *
 * @ingroup themeable
 */
function theme_password_hint_button($variables) {
  $element = $variables['element'];
  // We can't use type="submit" because that would alter the default submit
  // action of the form.
  $element['#attributes']['type'] = 'button';
  element_set_attributes($element, array('id', 'name', 'value'));
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  $el = '<input' . drupal_attributes($element['#attributes']) . ' />';
  return $el;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function password_hint_form_user_profile_form_alter(&$form, &$form_state) {
  $form['#validate'][] = 'password_hint_user_profile_form_validate';
}

/**
 * Validation function for the user profile form.
 */
function password_hint_user_profile_form_validate(&$form, &$form_state) {
  $account = $form['#user'];

  $old_hint = ($account->password_hint) ? $account->password_hint[LANGUAGE_NONE][0]['value'] : FALSE;
  $new_hint = $form_state['values']['password_hint'][LANGUAGE_NONE][0]['value'];

  if (!$new_hint) {
    return;
  }

  $actual_password = FALSE;
  if ($form_state['values']['pass']) {
    $actual_password = $form_state['values']['pass'];
  }
  elseif (isset($form_state['values']['current_pass'])) {
    $actual_password = $form_state['values']['current_pass'];
  }

  // The user will need to enter their password if they wish to change the hint.
  if ($old_hint != $new_hint) {
    // Skip the current password checks if the user is resetting their password.
    $valid_password = isset($_SESSION['pass_reset_' . $account->uid]) && isset($_GET['pass-reset-token']) && ($_GET['pass-reset-token'] == $_SESSION['pass_reset_' . $account->uid]);
    if (!$valid_password) {
      // Borrowed from user_validate_current_pass() - validate the user's
      // current password was correct.
      require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
      $valid_password = !empty($form_state['values']['current_pass']) && user_check_password($form_state['values']['current_pass'], $account);
      if (!$valid_password) {
        if ($actual_password) {
          $message = t("Your password incorrect; it's required to change the %name.", array('%name' => t('Password hint')));
        }
        else {
          $message = t("You must supply a password in order to change %name.", array('%name' => t('Password hint')));
        }

        // Match translation from user_validate_current_pass().
        form_set_error('current_pass', $message);
        form_set_error('password_hint');
        return;
      }
    }
  }

  // Also check that the hint doesn't contain the password.
  if (stripos($new_hint, $actual_password) !== FALSE) {
    form_set_error('password_hint', t("Your password hint must not contain your actual password"));
    return;
  }
}


/**
 * Insert an element after a specific point in an array with named keys.
 *
 * @param array $element
 *   The element to have $insert appended to.
 *
 * @param array $insert
 *   An array to be merged with $element.
 *
 * @param string $key
 *   The key within element that marks where $insert should be inserted. All
 *   elements in $insert will be added after $key
 */
function password_hint_element_insert_after(&$element, $insert, $key) {
  _password_hint_element_insert('after', $element, $insert, $key);
}

/**
 * Insert an element before a specific point in an array with named keys.
 *
 * @param array $element
 *   The element to have $insert appended to.
 *
 * @param array $insert
 *   An array to be merged with $element.
 *
 * @param string $key
 *   The key within element that marks where $insert should be inserted. All
 *   elements in $insert will be added before
 *   $key
 */
function password_hint_element_insert_before(&$element, $insert, $key) {
  _password_hint_element_insert('before', $element, $insert, $key);
}

/**
 * Insert an element at a specific point in an array.
 *
 * @see password_hint_element_insert_after()
 * @see password_hint_element_insert_before()
 */
function _password_hint_element_insert($mode, &$element, $insert, $key) {
  $new = array();
  while (list($k, $v) = each($element)) {
    if ($mode === 'after') {
      $new[$k] = $v;
    }
    if ($k === $key) {
      $element = $new + $insert + $element;
      return;
    }
    if ($mode === 'before') {
      $new[$k] = $v;
    }
  }
}
