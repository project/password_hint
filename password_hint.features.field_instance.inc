<?php
/**
 * @file
 * password_hint.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function password_hint_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-password_hint'
  $field_instances['user-user-password_hint'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter a hint that will help you to remember your password. Make sure you choose a hint that won\'t make it easy for other people to guess your password.<br />
 For security you\'ll need to enter your current password before you can set your password hint.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'password_hint',
    'label' => 'Password hint',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Enter a hint that will help you to remember your password. Make sure you choose a hint that won\'t make it easy for other people to guess your password.<br />
 For security you\'ll need to enter your current password before you can set your password hint.');
  t('Password hint');

  return $field_instances;
}
