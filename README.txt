This module depends on the features module to provide users with a password hint
field. Once set, they may request a hint when logging in if they can't remember
their password.

Security features

Obviously this module brings security into question. Allowing users to set a
password hint provides them with an opportunity to create their own security
weaknesses. Before you install this module you will need to consider a) how
trustworthy your users are and b) the implications of a compromise on one of
your accounts.

With that in mind, there are some mitigations to guard against possible attacks:

    Flood control to reduce the effectiveness of brute force username attacks.
    Your password is required to change the hint. Your hint may not be the same
    as your password.
