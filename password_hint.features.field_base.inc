<?php
/**
 * @file
 * password_hint.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function password_hint_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'password_hint'
  $field_bases['password_hint'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'user',
    ),
    'field_name' => 'password_hint',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
